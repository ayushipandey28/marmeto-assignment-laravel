<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Auth::routes(['verify' => true]);

Route::group(['middleware' => ['auth']], function () {

    Route::get('/', 'HomeController@index')->name('home');
    Route::get('/product/add', 'ProductsController@addProduct')->name('add-product');
    Route::post('/product/add', 'ProductsController@storeProduct')->name('add-product');  
    Route::get('/product/list', 'ProductsController@showProducts')->name('view-products');
});
