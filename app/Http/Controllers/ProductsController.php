<?php

namespace App\Http\Controllers;

use App\Models\Products;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeProduct(Request $request)
    { 
          
        $validator = Validator::make($request->all(), [
            'product_name' => 'required|string|max:255',
            'product_description' => 'required|string|max:255',
            'product_price' => 'required|integer',

        ]);   

        if ($validator->fails())
        {
            return redirect()->back()->withErrors($validator->errors());
        }
        else
        {
                    
            $productData = array(
            'product_name'  => $request->get('product_name'),
            'product_description'  => $request->get('product_description'),
            'product_price' => $request->get('product_price'),
            'created_by' => Auth::id()
            );

            $product = Products::insert($productData);
            return redirect(route('add-product'));
        }
        
    }

    public function addProduct(Request $request)
    {
        //
        return view('product.product-add');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Products  $products
     * @return \Illuminate\Http\Response
     */
    public function showProducts(Products $products)
    {
        //
        $user = Auth::id();
        $productList = Products::select('product_name','product_description','product_price','created_at','updated_at')
                        ->where('created_by',$user)->get();
               
       return view('product.product-list',compact('productList'));               
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Products  $products
     * @return \Illuminate\Http\Response
     */
    public function edit(Products $products)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Products  $products
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Products $products)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Products  $products
     * @return \Illuminate\Http\Response
     */
    public function destroy(Products $products)
    {
        //
    }
}
