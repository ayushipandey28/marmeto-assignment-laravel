<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Auth;
 
class RegisterControllerOld extends Controller
{
    function index()
    {
     return view('register.register');
    }

    function validateRegistration(Request $request)
    {
     $this->validate($request, [
      'email'   => 'required|email',
      'password'  => 'required|alphaNum|min:3',
      'name' => 'required'
     ]);

     $user_data = array(
      'name'  => $request->get('email'),
      'email'  => $request->get('email'),
      'password' => $request->get('password'),
      'confirm-password' => $request->get('confirm-password')

     );

     if(Auth::attempt($user_data))
     {
      return redirect('user/profile');
     }
     else
     {
      return back()->with('error', 'Wrong Login Details');
     }

    }

}
?>