@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Product List</div>

                <div class="card-body">
                @if(count($productList))
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>Product Name</th>
                        <th>Description</th>
                        <th>Price</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach($productList as $product)
                        <tr>
                            <td>{{$product->product_name}}</td>
                            <td>{{$product->product_description}}</td>
                            <td>{{$product->product_price}}</td>
                        </tr>     
                        @endforeach               
                    </tbody>
                </table>
                @else
                <h4>No Products to show <a href="{{route('add-product')}}">Add Here</a></h4>
                @endif

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
